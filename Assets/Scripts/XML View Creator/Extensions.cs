﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using Newtonsoft.Json;

/// <summary>
/// Class with Extensions and Useful methods.
/// </summary>
public static class Extensions {

	/// <summary>
	/// Extension. Returns XmlAttributeCollection as List.
	/// </summary>
	/// <param name="list"></param>
	/// <returns>Return List of XmlAttribute.</returns>
	public static List<XmlAttribute> ToList (this XmlAttributeCollection list) {
		List<XmlAttribute> output = new List<XmlAttribute> ();
		foreach (XmlAttribute item in list) {
			output.Add (item);
		}
		return output;
	}

	/// <summary>
	/// Extension. Returns List of Children.
	/// </summary>
	/// <param name="transform"></param>
	/// <returns>Transform list of children.</returns>
	public static List<Transform> GetChildren (this Transform transform) {
		List<Transform> output = new List<Transform> ();
		for (int i = 0; i < transform.childCount; i++) {
			output.Add (transform.GetChild (i));
		}
		return output;
	}

	/// <summary>
	/// Returns List of T Component, which are attached to children.
	/// </summary>
	/// <typeparam name="T">Type of Component</typeparam>
	/// <param name="transform"></param>
	/// <returns>List of Components - they are taken from each child if it's not null.</returns>
	public static List<T> GetComponentFromAllChildren<T> (this Transform transform) where T : Component {
		List<T> output = new List<T> ();
		for (int i = 0; i < transform.childCount; i++) {
			if(transform.GetChild (i).GetComponent<T> () != null)
				output.Add (transform.GetChild (i).GetComponent<T> ());
		}
		return output;
	}

	/// <summary>
	/// Returns value of enum based on it's string value.
	/// </summary>
	/// <typeparam name="T">Enum Type.</typeparam>
	/// <param name="value">String value.</param>
	/// <returns>Enum Value.</returns>
	/// <exception cref="ArgumentException">Throws when T is not enum or the given enum doesn't contains given value.</exception>
	public static T GetValue<T> (string value) where T : struct, IConvertible {
		if (!typeof (T).IsEnum) throw new ArgumentException ("T has to be Enum.");
		var temp = (Enum.GetNames (typeof (T))).ToList ();
		if (temp.Any (x => x.ToLower () == value.ToLower ())) {
			return (T) Enum.GetValues (typeof (T)).GetValue (temp.IndexOf (temp.FirstOrDefault (x => x.ToLower () == value.ToLower ())));
		} else throw new ArgumentException ($"The value doesn't contains the right value! {value} {temp[0]}");
	}

	/// <summary>
	/// Extension. Allows to add to Vector2 Vector3. Z dimension is ignored.
	/// </summary>
	/// <param name="vector2"></param>
	/// <param name="vector3">Vector to be added.</param>
	/// <returns>Added Vector.</returns>
	public static Vector2 AddVector3 (this Vector2 vector2, Vector3 vector3) {
		return vector2 + new Vector2 (vector3.x, vector3.y);
	}

	/// <summary>
	/// Extension. Copy string value to destination.
	/// </summary>
	/// <param name="s"></param>
	/// <param name="sourceIndex">Start index of coping origin value.</param>
	/// <param name="destination">String to which will be value copied.</param>
	/// <param name="destinationIndex">Copy string start index.</param>
	/// <param name="count">Count of chars to be copied.</param>
	public static void CopyTo (this string s, int sourceIndex, string destination, int destinationIndex, int count) {
		char[] vs = new char[s.Length];
		s.CopyTo (sourceIndex, vs, destinationIndex, count);
		destination = string.Join ("", vs);
	}
	/// <summary>
	/// Copies value of string and returns it.
	/// </summary>
	/// <param name="s"></param>
	/// <returns>Copy of string.</returns>
	public static string Copy (this string s) {
		string output = "";
		s.CopyTo (0, output, 0, s.Length);
		return output;
	}

}