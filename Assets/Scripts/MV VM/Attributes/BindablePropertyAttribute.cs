﻿using System;
using System.Runtime.CompilerServices;

/// <summary>
/// Attribute which marks property, which may be binded.
/// </summary>
[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
sealed class BindablePropertyAttribute : Attribute {

	/// <summary>
	/// Contains name of property.
	/// </summary>
	public string PropertyName { get; private set; }

	/// <summary>
	/// Marks the Property as Bindable.
	/// </summary>
	/// <remarks> Thanks to that it is possible to found by Binding Manager.</remarks>
	/// <param name="name">Name of property.</param>
	public BindablePropertyAttribute ([CallerMemberName] string name = "") {
		PropertyName = name;
	}

}