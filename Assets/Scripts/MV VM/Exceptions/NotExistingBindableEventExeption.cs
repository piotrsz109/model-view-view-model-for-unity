﻿using System;

/// <summary>
/// Represents Exception, when given Bindable Event doesn't exists.
/// </summary>
[Serializable]
public class NotExistingBindableEventException : Exception {
	
	/// <summary>
	/// Creates the Exception.
	/// </summary>
	public NotExistingBindableEventException () { }
	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="message">Message, about exception.</param>
	public NotExistingBindableEventException (string message) : base (message) { }
	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="message">Message, about exception.</param>
	/// <param name="inner">Exception, which is inner.</param>
	public NotExistingBindableEventException (string message, Exception inner) : base (message, inner) { }
	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="info">Info, about exception.</param>
	/// <param name="context">Context of exception.</param>
	protected NotExistingBindableEventException (
	  System.Runtime.Serialization.SerializationInfo info,
	  System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
}