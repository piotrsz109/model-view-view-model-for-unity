﻿using UnityEngine;

/// <summary>
/// Class represents Panel as UIControl.
/// </summary>
public class Panel : UIControl {

	/// <summary>
	/// Orientation of Panel.
	/// </summary>
	[SerializeField] protected Orientation orientation = Orientation.Vertical;

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		Orientation = Extensions.GetValue<Orientation> ((string) properties["orientation"]);
	}

	/// <summary>
	/// Bindable Property. Panel's Orientation.
	/// </summary>
	[BindableProperty]
	public Orientation Orientation {
		get { return orientation; }
		set {
			orientation = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
			SetLayout ();
		}
	}

	/// <summary>
	/// Overwritten inherited method. Setups position for all children..
	/// </summary>
	public override void SetLayout () {
		base.SetLayout ();
		if (Children == null || Children.Length == 0) return;
		for (int i = 0; i < Children.Length; i++) {
			if (i > 0)
				Children[i].Position = CalculatePosition (Children[i].transform, Children[i - 1].transform);
			else Children[i].Position = CalculatePosition (Children[i].transform, null);
		}
	}

	/// <summary>
	/// Calculates the position for given RectTransform. It's related to previous RectTransform and it's depended from orientation.
	/// </summary>
	/// <param name="rect">RectTransform of object for which position is calculating.</param>
	/// <param name="lastRect">Previous  game object's RectTransfrom.</param>
	/// <returns>Returns the calculated position in 2D space.</returns>
	private Vector2 CalculatePosition (RectTransform rect, RectTransform lastRect) {
		var rectControl = rect.GetComponent<UIControl> ();
		var lastRectControl = lastRect?.GetComponent<UIControl> ();
		Vector2 output = rect.sizeDelta / 2f;
		if (orientation == Orientation.Vertical)
			output += CalculateVerticalMargins (rectControl, lastRectControl);
		else output += CalculateHorizontalMargins (rectControl, lastRectControl);
		return output;
	}

	/// <summary>
	/// Adds specific margins of controls to calculate position for Vertical Orientation.
	/// </summary>
	/// <param name="control">UIControl for which position is calculating.</param>
	/// <param name="lastControl">Previous UIControl.</param>
	/// <returns>Returns in Vector2 added margins.</returns>
	private Vector2 CalculateVerticalMargins (UIControl control, UIControl lastControl) {
		Vector2 output = Vector2.zero;
		output += new Vector2 (control.LeftMargin, control.TopMargin);
		if (lastControl != null) {
			output += new Vector2 (0, lastControl.BottomMargin);
			output += new Vector2 (0, -lastControl.Position2D.y + lastControl.Size.y / 2f);
		}
		return output;
	}

	/// <summary>
	/// Adds specific margins of controls to calculate position for Horizontal Orientation.
	/// </summary>
	/// <param name="control">UIControl for which position is calculating.</param>
	/// <param name="lastControl">Previous UIControl.</param>
	/// <returns>Returns in Vector2 added margins.</returns>
	private Vector2 CalculateHorizontalMargins (UIControl rectControl, UIControl lastRectControl) {
		Vector2 output = Vector2.zero;
		output += new Vector2 (rectControl.LeftMargin, rectControl.TopMargin);
		if (lastRectControl != null) {
			output += new Vector2 (lastRectControl.RightMargin, 0);
			output += new Vector2 (lastRectControl.Position2D.x + lastRectControl.Size.x / 2f, 0);
		}
		return output;
	}

}

/// <summary>
/// Orientation Enum.
/// </summary>
public enum Orientation {
	Horizontal,
	Vertical
}