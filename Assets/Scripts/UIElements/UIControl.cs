﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Base class for all UIControls.
/// </summary>
public class UIControl : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	/// <summary>
	/// Properties from CSS, which will be applied to UIControl.
	/// </summary>
	[SerializeField] protected Dictionary<string, object> properties = new Dictionary<string, object> ();

	private float topMargin;
	private float leftMargin;
	private float bottomMargin;
	private float rightMargin;
	private float width;
	private float height;
	/// <summary>
	/// Does scaling is requested.
	/// </summary>
	private bool useScaling = false;
	/// <summary>
	/// Does scaling is applied.
	/// </summary>
	private bool appliedScalling = false;

	/// <summary>
	/// Bindable Property. Do UIControl Graphics, and it's children Graphics are disabled?
	/// </summary>
	[BindableProperty]
	public bool Visible {
		get {
			return (bool) GetComponent<MaskableGraphic> ()?.enabled;
		}
		set {
			foreach (var item in GetComponents<MaskableGraphic> ()) {
				item.enabled = value;
			}
			foreach (var item in GetComponentsInChildren<MaskableGraphic> ()) {
				item.enabled = value;
			}
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	/// <summary>
	/// Bindable Property. UIControl's name, and as such game object's name.
	/// </summary>
	[BindableProperty]
	public string Name {
		set {
			gameObject.name = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
		get {
			if (gameObject == null || this == null) return null;
			return gameObject.name;
		}
	}
	/// <summary>
	/// Bindable Property. UIControl's Background color. Don't works on Image Control.
	/// </summary>
	[BindableProperty]
	public SerializableColor BackgroundColor {
		get {
			var output = new SerializableColor (GetComponent<UnityEngine.UI.Image> ()?.color);
			if (this is Slider)
				output = new SerializableColor (transform.GetChild (0).GetComponent<UnityEngine.UI.Image> ().color);
			return output;
		}
		set {
			if (this is Slider) {
				transform.GetChild (0).GetComponent<UnityEngine.UI.Image> ().color = value.GetColor ();
			} else if (this is Image)
				return;
			else {
				GetComponent<UnityEngine.UI.Image> ().color = value.GetColor ();
			}
		}
	}
	/// <summary>
	/// Bindable Property. UIControl transform's delta size.
	/// </summary>
	[BindableProperty]
	public Vector2 Size {
		get {
			return transform.sizeDelta;
		}
		set {
			transform.sizeDelta = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Control's Anchored position.
	/// </summary>
	public Vector3 Position {
		get { return transform.position; }
		set {
			value.y *= -1f;
			transform.anchoredPosition3D = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Control's Anchored position.
	/// </summary>
	public Vector2 Position2D {
		get { return transform.anchoredPosition; }
		set { transform.anchoredPosition = value; }
	}
	/// <summary>
	/// Overwriting inherited transform property.
	/// </summary>
	#pragma warning disable IDE1006 // Naming Styles
	[BindableProperty]
	public new RectTransform transform {
		get { return GetComponent<RectTransform> (); }
	}
	/// <summary>
	/// Returns UIControl's Parent.
	/// </summary>
	public UIControl Parent {
		get {
			var output = transform.parent.GetComponent<UIControl> ();
			if (output == null)
				output = transform.parent.GetComponentInParent<UIControl> ();
			return output;
		}
	}
	/// <summary>
	/// Returns UIControl's children.
	/// </summary>
	public UIControl[] Children {
		get {
			var output = transform.GetComponentFromAllChildren<UIControl> ();
			if (this is ScrollView)
				output = transform.GetChild (0).GetChild (0).GetComponentFromAllChildren<UIControl> ();
			return output.ToArray();
		}
	}
	/// <summary>
	/// Bindable Property. Top Margin in pixels.
	/// </summary>
	[BindableProperty]
	public float TopMargin {
		get { return topMargin; }
		set {
			topMargin = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Bindable Property. Bottom Margin in pixels.
	/// </summary>
	[BindableProperty]
	public float BottomMargin {
		get { return bottomMargin; }
		set {
			bottomMargin = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Bindable Property. Left Margin in pixels.
	/// </summary>
	[BindableProperty]
	public float LeftMargin {
		get { return leftMargin; }
		set {
			leftMargin = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Bindable Property. Right Margin in pixels.
	/// </summary>
	[BindableProperty]
	public float RightMargin {
		get { return rightMargin; }
		set {
			rightMargin = value;
			SetLayout ();
		}
	}
	/// <summary>
	/// Bindable Property. Control's width in pixels.
	/// </summary>
	[BindableProperty]
	public float Width {
		get { return width; }
		set {
			width = value;
			Size = new Vector2 (value, Size.y);
			SetLayout ();
		}
	}
	/// <summary>
	/// Bindable Property. Control's height in pixels.
	/// </summary>
	[BindableProperty]
	public float Height {
		get { return height; }
		set {
			height = value;
			Size = new Vector2 (Size.x, value);
			SetLayout ();
		}
	}

	/// <summary>
	/// Delegate which represents mouse event handler.
	/// </summary>
	/// <param name="sender">Object which raises this event.</param>
	public delegate void PointerEvent(object sender);
	
	/// <summary>
	/// Bindable Event. Event is invoked when mouse clicks the UIControl.
	/// </summary>
	[BindableEvent]
	public event PointerEvent OnMouseClick;
	/// <summary>
	/// Bindable Event. Event is invoked when mouse enters the UIControl's rectangle.
	/// </summary>
	[BindableEvent]
	public event PointerEvent OnMouseEnter;
	/// <summary>
	/// Bindable Event. Event is invoked when mouse exits the UIControl's rectangle.
	/// </summary>
	[BindableEvent]
	public event PointerEvent OnMouseExit;

	/// <summary>
	/// Method, which is called by CSSParser. Sets properties, and sets, does the scaling is requested.
	/// </summary>
	/// <param name="properties">Reference of dictionary which properties values.</param>
	/// <param name="useScaling">Does scaling is requested. scaling</param>
	public void SetupProperties (ref Dictionary<string, object> properties, bool useScaling) {
		this.properties = properties;
		SetupStyle ();
		this.useScaling = useScaling;
	}

	/// <summary>
	/// Method sets UIControl's appearance.
	/// </summary>
	protected void SetupStyle () {
		SetupStyleData ();
		SetupCharacteristicStyle ();
	}

	/// <summary>
	/// Sets the values which all UIControls inherits.
	/// </summary>
	protected void SetupStyleData () {
		BackgroundColor = properties["background-color"] as SerializableColor;
		width = (float) properties["width"];
		height = (float) properties["height"];
		Visible = (bool) properties["visible"];
		topMargin = (float) properties["margin-top"];
		leftMargin = (float) properties["margin-left"];
		bottomMargin = (float) properties["margin-bottom"];
		rightMargin = (float) properties["margin-right"];
		Size = new Vector2 (width, height);
	}

	/// <summary>
	/// Virtual method, which other UIControls overwrite. Is called by SetupStyle method.
	/// </summary>
	protected virtual void SetupCharacteristicStyle () { }

	/// <summary>
	/// Virtual method, which is overwrite by UIControls, which might contain other UIControls.
	/// </summary>
	public virtual void SetLayout () {
		ScaleGlobalMarginsAndSizeToLocal ();
		if (!(this is ScrollView || this is Panel || this is Window)) return;
	}

	/// <summary>
	/// Scales margins and size, based on parents size and canvas size.
	/// </summary>
	public void ScaleGlobalMarginsAndSizeToLocal () {
		if (!useScaling || appliedScalling) return;
		appliedScalling = true;
		Vector2 aspect = Parent.Size / Window.CanvasDeltaSize;
		transform.sizeDelta *= aspect;
		leftMargin *= aspect.x;
		rightMargin *= aspect.x;
		topMargin *= aspect.y;
		bottomMargin *= aspect.y;
		if (Children == null || Children.Length == 0)
			return;
		foreach (var item in Children) {
			item.ScaleGlobalMarginsAndSizeToLocal ();
		}
	}

	/// <summary>
	/// Implementation the interface: IPointerClickHandler.
	/// </summary>
	/// <param name="eventData">Contains the mouse position and delta.</param>
	public void OnPointerClick (PointerEventData eventData) {
		var temp = this;
		OnMouseClick?.Invoke (temp);
	}

	/// <summary>
	/// Implementation the interface: IPointerEnterHandler.
	/// </summary>
	/// <param name="eventData">Contains the mouse position and delta.</param>
	public void OnPointerEnter (PointerEventData eventData) {
		OnMouseEnter?.Invoke (this);
	}

	/// <summary>
	/// Implementation the interface: IPointerExitHandler.
	/// </summary>
	/// <param name="eventData">Contains the mouse position and delta.</param>
	public void OnPointerExit (PointerEventData eventData) {
		OnMouseExit?.Invoke (this);
	}

}