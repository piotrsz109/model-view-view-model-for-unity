﻿using System;
using System.Runtime.CompilerServices;

/// <summary>
/// Attribute, which marks event, to which you can bind handler.
/// </summary>
[AttributeUsage (AttributeTargets.Event, Inherited = false, AllowMultiple = true)]
sealed class BindableEventAttribute : Attribute {

	/// <summary>
	/// Contains event name.
	/// </summary>
	public string EventName { get; private set; }
	
	/// <summary>
	/// Marks event as Bindable.
	/// </summary>
	/// <remarks>Thanks to that it's possible that Binding Manager will find it.</remarks>
	/// <param name="name">Name of Event.</param>
	public BindableEventAttribute ([CallerMemberName] string name = "") {
		EventName = name;
	}

}