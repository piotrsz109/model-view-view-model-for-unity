﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents ComboBox as UIControl.
/// </summary>
public class ComboBox : UIControl {

	/// <summary>
	/// ComboBox Item template Text Component.
	/// </summary>
	Text templateText;
	/// <summary>
	/// ComboBox item selected Text Component.
	/// </summary>
	Text selectedText;
	/// <summary>
	/// Reference to Dropdown script.
	/// </summary>
	Dropdown dropdown;

	/// <summary>
	/// Delegate which represents OnItemCollectionChanged event handler.
	/// </summary>
	/// <param name="sender">Object which Invokes event.</param>
	/// <param name="newItems">ICollection with current Items.</param>
	public delegate void ItemCollectionChanged (object sender, ICollection<Dropdown.OptionData> newItems);

	/// <summary>
	/// Bindable Event. It's invoked when ComboBox's items, have changed.
	/// </summary>
	[BindableEvent]
	public event ItemCollectionChanged OnItemCollectionChanged;

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		SetupTextStyle ();
		var image = transform.GetChild (2).GetChild (0).GetChild (0).GetChild (0).GetChild (0).GetComponent<UnityEngine.UI.Image> ();
		GetComponent<UnityEngine.UI.Image>().color = image.color = (properties.ElementAt (0).Value as SerializableColor).GetColor ();
	}

	/// <summary>
	/// Sets reference and Text Components.
	/// </summary>
	protected void SetupTextStyle () {
		templateText = transform.GetChild (2).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetComponent<Text> ();
		selectedText = transform.GetChild (0).GetComponent<Text> ();
		dropdown = GetComponent<Dropdown> ();
		Items.Clear ();
	}

	/// <summary>
	/// Read Only property. Selected in ComboBox OptionData.
	/// </summary>
	public Dropdown.OptionData Selected {
		get { return dropdown.options[dropdown.value]; }
	}

	/// <summary>
	/// Bindable Property. ComboBox'es Item Collection. Returns ObserverableCollection.
	/// </summary>
	[BindableProperty]
	public ICollection<Dropdown.OptionData> Items {
		get {
			ObservableCollection<Dropdown.OptionData> output = new ObservableCollection<Dropdown.OptionData>(dropdown.options);
			output.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) =>
			{
				OnItemCollectionChanged (this, output);
				Window.BindingManager.CallBindingPropetyValueChange (this,false, "Items");
			};
			return dropdown.options;
		}
		set {
			dropdown.options = new List<Dropdown.OptionData> (value.ToArray());
			OnItemCollectionChanged (this, dropdown.options);
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Texts' Font. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return templateText.font; }
		set {
			templateText.font = selectedText.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Texts' text size.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return templateText.fontSize; }
		set {
			templateText.fontSize = selectedText.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Text's text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return templateText.fontStyle; }
		set {
			templateText.fontStyle = selectedText.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Texts' text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor (templateText.color); }
		set {
			templateText.color = selectedText.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Texts' text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return templateText.alignment; }
		set {
			templateText.alignment = selectedText.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}