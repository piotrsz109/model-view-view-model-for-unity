﻿
/// <summary>
/// Class which represents Slider as UIControl.
/// </summary>
public class Slider : UIControl {

	/// <summary>
	/// Slider's thumb Image reference.
	/// </summary>
	private UnityEngine.UI.Image thumb;
	/// <summary>
	/// Slider's filled Image reference.
	/// </summary>
	private UnityEngine.UI.Image filled;
	/// <summary>
	/// Slider script reference.
	/// </summary>
	private UnityEngine.UI.Slider slider;

	/// <summary>
	/// Delegate which represents the OnSliderValueChanged event handler.
	/// </summary>
	/// <param name="sender">Object which Invokes the event.</param>
	/// <param name="value">Current Slider Value.</param>
	public delegate void SliderValueChanged (object sender, float value);

	/// <summary>
	/// Bindable Event. It's invoked when slider value has changed.
	/// </summary>
	[BindableEvent]
	public event SliderValueChanged OnSliderValueChanged;

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		thumb = transform.GetChild (2).GetChild (0).GetComponent<UnityEngine.UI.Image> ();
		filled = transform.GetChild (1).GetChild (0).GetComponent<UnityEngine.UI.Image> ();
		slider = GetComponent<UnityEngine.UI.Slider> ();
		slider.onValueChanged.AddListener ((float value) => { OnSliderValueChanged?.Invoke (this, value); });
	}
	
	/// <summary>
	/// Bindable Property. Value of slider.
	/// </summary>
	[BindableProperty]
	public float Value {
		get { return GetComponent<UnityEngine.UI.Slider> ().value; }
		set { GetComponent<UnityEngine.UI.Slider> ().value = value; }
	}

	/// <summary>
	/// Bindable Property. Color of Filled part of Slider.
	/// </summary>
	[BindableProperty]
	public SerializableColor FilledColor {
		get { return new SerializableColor (filled.color); }
		set {
			filled.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property Color of Slider's Thumb.
	/// </summary>
	[BindableProperty]
	public SerializableColor ThumbColor {
		get { return new SerializableColor (thumb.color); }
		set {
			thumb.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Minimum value of Slider.
	/// </summary>
	[BindableProperty]
	public float Minimum {
		get { return slider.minValue; }
		set {
			slider.minValue = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Maximum value of Slider.
	/// </summary>
	[BindableProperty]
	public float Maximum {
		get { return slider.maxValue; }
		set {
			slider.maxValue = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}