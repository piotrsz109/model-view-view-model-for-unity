﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents Toggle as UIControl.
/// </summary>
public class Toggle : UIControl {

	/// <summary>
	/// Reference to Text Component
	/// </summary>
	private Text text;
	/// <summary>
	/// Reference to UnityEngine.UI Toggle script.
	/// </summary>
	private UnityEngine.UI.Toggle toggle;
	/// <summary>
	/// Delegate which represents OnToggleStateChanged event handler.
	/// </summary>
	/// <param name="sender">Object which Invokes the event.</param>
	/// <param name="state">Current value.</param>
	public delegate void ToogleStateChange (object sender, bool state);

	/// <summary>
	/// Bindable Event. It's invoked when Toggle changes the value.
	/// </summary>
	[BindableEvent]
	public event ToogleStateChange OnToggleStateChanged;

	/// <summary>
	/// It's called on Awake. 
	/// </summary>
	private void Awake () {
		toggle = GetComponent<UnityEngine.UI.Toggle> ();
		toggle.onValueChanged.AddListener ((bool t) => { OnToggleStateChanged?.Invoke (this, t); });
	}

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		text = GetComponentInChildren<Text> ();
		Text = (string) properties["text"];
		FontFamily = Resources.FindObjectsOfTypeAll<Font> ().FirstOrDefault (x => x.name == (string) properties["font-family"]);
		FontSize = (int) (float) properties["font-size"];
		FontDecoration = Extensions.GetValue<FontStyle> ((string) properties["font-decoration"]);
		TextAlignment = Extensions.GetValue<TextAnchor> ((string) properties["text-alignment"]);
		Color = (SerializableColor) properties["color"];
	}

	/// <summary>
	/// Bindable Property. Toggle's Text.
	/// </summary>
	[BindableProperty]
	public string Text {
		get { return text.text; }
		set {
			string oldValue = text.text;
			text.text = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	
	/// <summary>
	/// Bindable Property. Toggle's text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return text.font; }
		set {
			text.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	
	/// <summary>
	/// Bindable Property. Toggle's text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return text.fontSize; }
		set {
			text.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	
	/// <summary>
	/// Bindable Property. Toggle's text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return text.fontStyle; }
		set {
			text.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	
	/// <summary>
	/// Bindable Property. Toggle's text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor (text.color); }
		set {
			text.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}
	
	/// <summary>
	/// Bindable Property. Toggle's text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return text.alignment; }
		set {
			text.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Toggle's value.
	/// </summary>
	[BindableProperty]
	public bool Value {
		get { return toggle.isOn; }
		set {
			toggle.isOn = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}