﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents Image as UIControl.
/// </summary>
public class Image : UIControl {

	/// <summary>
	/// Contains path to image.
	/// </summary>
	protected string source;

	/// <summary>
	/// Overwritten inherited method. Setups Source.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		Source = (string) properties["source"];
	}

	/// <summary>
	/// Bindable Property. Path to image in Resources folder or it's sub folder.
	/// </summary>
	/// <exception cref="System.ArgumentException">Throws when there's no existing image.</exception>
	[BindableProperty]
	public string Source {
		get { return source; }
		set {
			source = value;
			if (Resources.Load<Texture> (value) == null)
				throw new System.ArgumentException ("File with given path doesn't exists!");
			GetComponent<RawImage> ().texture = Resources.Load<Texture> (value);
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}