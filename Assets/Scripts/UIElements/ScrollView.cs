﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents ScrollView as UIControl. Not ready yet.
/// </summary>
public class ScrollView : UIControl {

	private int contentMargin;
	private RectTransform ContentTransform;

	/// <summary>
	/// Delegate which represents OnAddControl event handler.
	/// </summary>
	/// <param name="sender">Object, which Invokes that event.</param>
	/// <param name="control">GameObject of added Control.</param>
	public delegate void AddControlModification (object sender, GameObject control);
	/// <summary>
	/// Delegate which represents OnRemoveControls event handler.
	/// </summary>
	/// <param name="sender">Object which Invoke that event.</param>
	public delegate void RemoveControlsModification (object sender);

	/// <summary>
	/// Bindable Event. It's Invoked when new Control is added.
	/// </summary>
	[BindableEvent]
	public event AddControlModification OnAddControl;
	/// <summary>
	/// Bindable Event. It's Invoked when all Children are removed.
	/// </summary>
	[BindableEvent]
	public event RemoveControlsModification OnRemoveControls;

	/// <summary>
	/// Reference to UnityEngine Image of Thumb of Vertical ScrollBar.
	/// </summary>
	UnityEngine.UI.Image verticalScrollBarThumb;
	/// <summary>
	/// Reference to UnityEngine Image of Thumb of Horizontal ScrollBar.
	/// </summary>
	UnityEngine.UI.Image horizontalScrollBarThumb;
	/// <summary>
	/// Reference to ScrollRect script.
	/// </summary>
	ScrollRect scrollRect;

	/// <summary>
	/// Bindable Property. Thumbs' color.
	/// </summary>
	[BindableProperty]
	public SerializableColor ThumbColor {
		get { return new SerializableColor (verticalScrollBarThumb.color); }
		set {
			verticalScrollBarThumb.color = value.GetColor ();
			horizontalScrollBarThumb.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Vertical ScrollBar Visibility.
	/// </summary>
	[BindableProperty]
	public ScrollRect.ScrollbarVisibility VerticalScrollbarVisibility {
		get { return scrollRect.verticalScrollbarVisibility; }
		set {
			scrollRect.verticalScrollbarVisibility = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Horizontal ScrollVar Visibility.
	/// </summary>
	[BindableProperty]
	public ScrollRect.ScrollbarVisibility HorizontalScrollbarVisibility {
		get { return scrollRect.horizontalScrollbarVisibility; }
		set {
			scrollRect.horizontalScrollbarVisibility = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Margin in between two added controls. 
	/// </summary>
	[BindableProperty]
	public int ContentMargin {
		get { return contentMargin; }
		set {
			contentMargin = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		verticalScrollBarThumb = transform.GetChildren ().First (x => x.name == "Scrollbar Vertical").GetChild (0).GetChild (0).GetComponent<UnityEngine.UI.Image> ();
		horizontalScrollBarThumb = transform.GetChildren ().First (x => x.name == "Scrollbar Horizontal").GetChild (0).GetChild (0).GetComponent<UnityEngine.UI.Image> ();
		scrollRect = GetComponent<ScrollRect> ();
		ThumbColor = (SerializableColor) properties["thumb-color"];
		VerticalScrollbarVisibility = Extensions.GetValue<ScrollRect.ScrollbarVisibility> ((string) properties["display-vertical-scrollbar"]);
		HorizontalScrollbarVisibility = Extensions.GetValue<ScrollRect.ScrollbarVisibility> ((string) properties["display-horizontal-scrollbar"]);
		ContentTransform = transform.GetChild (0).GetChild (0).GetComponent<RectTransform> ();
	}

	/// <summary>
	/// Add new Control.
	/// </summary>
	/// <param name="control">Control to add.</param>
	public void AddControl (GameObject control) {
		if (control.GetComponent<RectTransform> ())
			throw new Exception ("To ScrollView you can add only elements with RectTransform.");
		var rect = control.GetComponent<RectTransform> ();
		var content = transform.GetChild (0).GetChild (0).GetComponent<RectTransform> ();
		if (content.sizeDelta.x < rect.sizeDelta.x) content.sizeDelta = new Vector2 (rect.sizeDelta.x, content.sizeDelta.y);
			content.sizeDelta += new Vector2 (0, rect.sizeDelta.y + ContentMargin);
		rect.SetParent (content);
		OnAddControl (this, control);
	}

	/// <summary>
	/// Removes all Controls in children.
	/// </summary>
	public void RemoveControls () {
		var content = transform.GetChild (0).GetChild (0).GetComponent<RectTransform> ();
		for (int i = 0; i < content.childCount; i++) {
			Destroy (content.GetChild (i).gameObject);
		}
		content.sizeDelta = GetComponent<RectTransform> ().sizeDelta + new Vector2(-10, 0);
		OnRemoveControls (this);
	}

	/// <summary>
	/// Overwritten inherited method. Setups position for all children..
	/// </summary>
	public override void SetLayout () {
		base.SetLayout ();
		for (int i = 0; i < Children.Length; i++) {
			if (i > 0)
				Children[i].Position = CalculatePosition (Children[i].transform, Children[i - 1].transform);
			else Children[i].Position = CalculatePosition (Children[i].transform, null);
			ContentTransform.sizeDelta += new Vector2(0, Children[i].Size.y + Children[i].TopMargin + Children[i].BottomMargin);
		}
	}
	
	/// <summary>
	/// Calculates the position for given RectTransform. It's related to previous RectTransform.
	/// </summary>
	/// <param name="rect">RectTransform of object for which position is calculating.</param>
	/// <param name="lastRect">Previous  game object's RectTransfrom.</param>
	/// <returns>Returns the calculated position in 2D space.</returns>
	private Vector2 CalculatePosition (RectTransform rect, RectTransform lastRect) {
		var rectControl = rect.GetComponent<UIControl> ();
		var lastRectControl = lastRect?.GetComponent<UIControl> ();
		Vector2 output = rect.sizeDelta / 2f;
		output += CalculateVerticalMargins (rectControl, lastRectControl);
		return output;
	}

	/// <summary>
	/// Adds specific margins of controls to calculate position.
	/// </summary>
	/// <param name="control">UIControl for which position is calculating.</param>
	/// <param name="lastControl">Previous UIControl.</param>
	/// <returns>Returns in Vector2 added margins.</returns>
	private Vector2 CalculateVerticalMargins (UIControl rectControl, UIControl lastRectControl) {
		Vector2 output = Vector2.zero;
		output += new Vector2 (rectControl.LeftMargin, rectControl.TopMargin);
		if (lastRectControl != null) {
			output += new Vector2 (0, lastRectControl.BottomMargin);
			output += new Vector2 (0, lastRectControl.Position.y + lastRectControl.Size.y / 2f);
		}
		return output;
	}

}