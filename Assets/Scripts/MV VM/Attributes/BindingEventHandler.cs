﻿using System;
using System.Runtime.CompilerServices;

/// <summary>
/// Marks method, which will be added as handler of any Bindable Event.
/// </summary>
[AttributeUsage (AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
sealed class BindingEventHandlerAttribute : Attribute {

	/// <summary>
	/// Stores name of method, which is handler
	/// </summary>
	public string MethodName { get; private set; }
	/// <summary>
	/// Name of target event
	/// </summary>
	public string EventName { get; private set; }
	/// <summary>
	/// Name of UIControl, which contains that event
	/// </summary>
	public string ControlName { get; private set; }

	/// <summary>
	/// Marks the method, which will be handler of given event.
	/// </summary>
	/// <remarks>You can only Bind method to events marked by BindableEvent attribute.</remarks>
	/// <param name="controlName">Name of UIControl, which contains given event.</param>
	/// <param name="eventName">Event name.</param>
	/// <param name="name">Name of method.</param>
	public BindingEventHandlerAttribute (string controlName, string eventName, [CallerMemberName] string name = "") {
		ControlName = controlName;
		EventName = eventName;
		MethodName = name;
	}

}