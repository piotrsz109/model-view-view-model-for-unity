﻿using System;

/// <summary>
/// Represents Exception, when given Bindable Property doesn't exists.
/// </summary>
[Serializable]
public class NotExistingBindablePropertyException : Exception {

	/// <summary>
	/// Creates the Exception.
	/// </summary>
	public NotExistingBindablePropertyException () { }

	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="message">Message, about exception.</param>
	public NotExistingBindablePropertyException (string message) : base (message) { }

	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="message">Message, about exception.</param>
	/// <param name="inner">Exception, which is inner.</param>
	public NotExistingBindablePropertyException (string message, Exception inner) : base (message, inner) { }

	/// <summary>
	/// Creates the Exception.
	/// </summary>
	/// <param name="info">Info, about exception.</param>
	/// <param name="context">Context of exception.</param>
	protected NotExistingBindablePropertyException (
	  System.Runtime.Serialization.SerializationInfo info,
	  System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
}
