<h2>UPF - Unity Presentation Foundation</h2>
<h3>To make interface creatin very easy</h3>
<p>Welcome in my project. I've created UPF, to improve creation of UI in Unity.</p>
<p>Thanks to it, every Unity user might create interface, by just typing some XML.</p>
<p>XML Schema, which is linked to XML File, will make interface creation very simple.</p>
<p>Adding the event handlers of Interface Elements is easy. You just have to add Binding Event Handler attribute to method.
There you just pass the arguments.
<br/>To add Property Binding you just have to add BindingProperty attribute to property.
On property set, you should call the CallBindingPropetyValueChange method.</p>
It's part of Binding Manager, to which you can access by Window and static variable which reference to this script.</p>

<p>To make it work, you just have to create Canvas and attach to it XML Parser To UI script.
Then you have to write path to XML File, and add ViewModels as components</p>

Please look at example in Resources/Sample of UI.
<br/>