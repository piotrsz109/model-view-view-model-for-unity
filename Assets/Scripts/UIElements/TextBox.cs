﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents TextBox as UIControl.
/// </summary>
public class TextBox : UIControl {

	/// <summary>
	/// Delegate which represents the OnTextChanged event handler.
	/// </summary>
	/// <param name="sender">Object which Invokes the event.</param>
	/// <param name="oldText">Old value of TextBox.</param>
	/// <param name="newText">Current value of TextBox.</param>
	public delegate void TextChanged (object sender, string oldText, string newText);

	/// <summary>
	/// Bindable Event. It's invoked when TextBox value has changed.
	/// </summary>
	[BindableEvent]
	public event TextChanged OnTextChanged;

	/// <summary>
	/// Reference to Text Component.
	/// </summary>
	private Text text;
	/// <summary>
	/// Reference to Placeholder's Text Component.
	/// </summary>
	private Text placeholderText;

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		text = transform.GetChild (1).GetComponent<Text> ();
		placeholderText = transform.GetChild (0).GetComponent<Text> ();
		FontFamily = Resources.FindObjectsOfTypeAll<Font> ().FirstOrDefault (x => x.name == (string) properties["font-family"]);
		FontSize = (int) (float) properties["font-size"];
		FontDecoration = Extensions.GetValue<FontStyle> ((string) properties["font-decoration"]);
		TextAlignment = Extensions.GetValue<TextAnchor> ((string) properties["text-alignment"]);
		Color = (SerializableColor) properties["color"];
		PlaceholderColor = (SerializableColor) properties["placeholder-color"];
		Placeholder = (string) properties["placeholder"];
	}

	/// <summary>
	/// Bindable Property. TextBox'es text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return text.font; }
		set {
			placeholderText.font = text.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. TextBox'es text size.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return text.fontSize; }
		set {
			placeholderText.fontSize = text.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. TextBox'es text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return text.fontStyle; }
		set {
			text.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. TextBox'es text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor (text.color); }
		set {
			text.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. TextBox'es placeholder text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor PlaceholderColor {
		get { return new SerializableColor (placeholderText.color); }
		set {
			placeholderText.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. TextBox'es text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return text.alignment; }
		set {
			text.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Current Text Value.
	/// </summary>
	[BindableProperty]
	public string Text {
		get {
			return text.text;
		}
		set {
			string old = text.text;
			OnTextChanged?.Invoke (this, old, value);
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Placeholder Text.
	/// </summary>
	[BindableProperty]
	public string Placeholder {
		get { return placeholderText.text; }
		set {
			placeholderText.text = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}