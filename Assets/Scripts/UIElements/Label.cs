﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Label : UIControl {

	/// <summary>
	/// Reference to Text Component.
	/// </summary>
	Text text;

	/// <summary>
	/// Delegate, which represents OnTextChanged event handler.
	/// </summary>
	/// <param name="sender">Object which invokes this event.</param>
	/// <param name="oldvalue">Old text value.</param>
	/// <param name="newValue">Current text value.</param>
	public delegate void TextChanged (object sender, string oldvalue, string newValue);

	/// <summary>
	/// Bindable Event. It's invoked when Text property has changed.
	/// </summary>
	[BindableEvent]
	public event TextChanged OnTextChanged;

	/// <summary>
	/// Called on Awake. Sets text reference if it's null.
	/// </summary>
	private void Awake () {
		if(text == null)
			text = GetComponentInChildren<Text> ();
	}

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		text = GetComponentInChildren<Text> ();
		Text = (string) properties["text"];
		FontFamily = Resources.FindObjectsOfTypeAll<Font> ().FirstOrDefault (x => x.name.ToLower() == ((string) properties["font-family"]).ToLower());
		FontSize = (int) (float) properties["font-size"];
		FontDecoration = Extensions.GetValue<FontStyle> ((string) properties["font-decoration"]);
		TextAlignment = Extensions.GetValue<TextAnchor> ((string) properties["text-alignment"]);
		Color = (SerializableColor) properties["color"];
	}

	/// <summary>
	/// Bindable Property. Label's Text.
	/// </summary>
	[BindableProperty]
	public string Text {
		get { return text.text; }
		set {
			string oldValue = "";
			oldValue = text.text;
			text.text = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
			OnTextChanged?.Invoke (this, oldValue, text.text);
		}
	}

	/// <summary>
	/// Bindable Property. Label's text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return text.font; }
		set {
			text.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Label's text size.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return text.fontSize; }
		set {
			text.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Label's text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return text.fontStyle; }
		set {
			text.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Label's text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor(text.color); }
		set {
			text.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Label's text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return text.alignment; }
		set {
			text.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}