﻿using UnityEngine;

/// <summary>
/// UIControl which is attached to Canvas.
/// </summary>
public class Window : UIControl {

	/// <summary>
	/// Size of Canvas.
	/// </summary>
	public static Vector2 CanvasDeltaSize;
	/// <summary>
	/// Reference to Binding Manager which is attached to Canvas too.
	/// </summary>
	public static BindingManager BindingManager;

	/// <summary>
	/// Called OnValidate. Set's values.
	/// </summary>
	private void OnValidate () {
		CanvasDeltaSize = GetComponent<RectTransform> ().sizeDelta;
		BindingManager = GetComponent<BindingManager> ();
	}

	/// <summary>
	/// Overwritten inherited method. Setups position for all children..
	/// </summary>
	public override void SetLayout () {
		var children = transform.GetComponentFromAllChildren<RectTransform> ();
		for (int i = 0; i < children.Count; i++) {
			if (i > 0)
				children[i].position = CalculatePosition (children[i], children[i - 1]);
			else children[i].position = CalculatePosition (children[i]);
		}
	}

	/// <summary>
	/// Calculates the position for given RectTransform. It's related to previous RectTransform.
	/// </summary>
	/// <param name="rect">RectTransform of object for which position is calculating.</param>
	/// <param name="lastRect">Previous  game object's RectTransfrom.</param>
	/// <returns>Returns the calculated position in 2D space.</returns>
	private Vector2 CalculatePosition (RectTransform rect, RectTransform lastRect=null) {
		var rectControl = rect.GetComponent<UIControl> ();
		var lastRectControl = lastRect?.GetComponent<UIControl> ();
		Vector2 output = rect.sizeDelta / 2f;
		if(lastRect != null)
			output = output.AddVector3 (lastRect.position);
		output += CalculateMargins (rectControl, lastRectControl);
		return output;
	}

	/// <summary>
	/// Adds specific margins of controls to calculate position.
	/// </summary>
	/// <param name="control">UIControl for which position is calculating.</param>
	/// <param name="lastControl">Previous UIControl.</param>
	/// <returns>Returns in Vector2 added margins.</returns>
	private Vector2 CalculateMargins (UIControl rectControl, UIControl lastRectControl) {
		Vector2 output = Vector2.zero;
		output += new Vector2 (rectControl.LeftMargin, rectControl.TopMargin);
		if (lastRectControl != null)
			output += new Vector2 (lastRectControl.RightMargin, lastRectControl.BottomMargin);
		return output;
	}

}