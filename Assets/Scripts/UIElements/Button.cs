﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents UIControl as Button.
/// </summary>
public class Button : UIControl {

	/// <summary>
	/// Contains reference on Text Component.
	/// </summary>
	Text text;

	/// <summary>
	/// Bindable Property. Overwritten Background color.
	/// </summary>
	[BindableProperty]
	public new SerializableColor BackgroundColor {
		get {
			var output = new SerializableColor (GetComponent<UnityEngine.UI.Image> ()?.color);
			return output;
		}
		set {
			GetComponent<UnityEngine.UI.Image> ().color = value.GetColor ();
			SetupAllColors ();
		}
	}

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		text = GetComponentInChildren<Text> ();
		var fonts = Resources.FindObjectsOfTypeAll <Font> ();
		Text = (string) properties["text"];
		FontFamily = fonts.FirstOrDefault (x => x.name.ToLower() == ((string) properties["font-family"]).ToLower());
		FontSize = (int) (float) properties["font-size"];
		FontDecoration = Extensions.GetValue<FontStyle> ((string) properties["font-decoration"]);
		TextAlignment = Extensions.GetValue<TextAnchor> ((string) properties["text-alignment"]);
		Color = (SerializableColor) properties["color"];
	}

	/// <summary>
	/// Bindable Property. Button's Text.
	/// </summary>
	[BindableProperty]
	public string Text {
		get { return text.text; }
		set {
			string oldValue = text.text;
			text.text = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Button's text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return text.font; }
		set {
			text.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Button's text size.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return text.fontSize; }
		set {
			text.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Button's text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return text.fontStyle; }
		set {
			text.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Button's text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor (text.color); }
		set {
			text.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. Button's text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return text.alignment; }
		set {
			text.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Set's button normal, highlighted, and pressed color.
	/// </summary>
	private void SetupAllColors () {
		var button = GetComponent<UnityEngine.UI.Button> ();
		ColorBlock colorBlock = button.colors;
		colorBlock.normalColor = BackgroundColor.GetColor ();
		colorBlock.highlightedColor = (BackgroundColor + new SerializableColor ("#111111")).GetColor();
		colorBlock.pressedColor = (BackgroundColor - new SerializableColor ("#111111")).GetColor ();
		button.colors = colorBlock;
	}

}