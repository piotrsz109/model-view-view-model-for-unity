﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which represents PasswordBox as UIControl.
/// </summary>
public class PasswordBox : UIControl {

	/// <summary>
	/// The password Text Component
	/// </summary>
	Text text;
	/// <summary>
	/// The placeholder Text Component.
	/// </summary>
	Text placeholderText;
	/// <summary>
	/// InputField script reference.
	/// </summary>
	InputField inputField;

	/// <summary>
	/// Overwritten inherited method. Setups style.
	/// </summary>
	protected override void SetupCharacteristicStyle () {
		text = transform.GetChild (1).GetComponent<Text> ();
		placeholderText = transform.GetChild (0).GetComponent<Text> ();
		inputField = GetComponent<InputField> ();
		FontFamily = Resources.FindObjectsOfTypeAll<Font> ().FirstOrDefault (x => x.name.ToLower () == ((string) properties["font-family"]).ToLower ());
		FontSize = (int) (float) properties["font-size"];
		FontDecoration = Extensions.GetValue<FontStyle> ((string) properties["font-decoration"]);
		TextAlignment = Extensions.GetValue<TextAnchor> ((string) properties["text-alignment"]);
		Color = (SerializableColor) properties["color"];
		PlaceholderColor = (SerializableColor) properties["placeholder-color"];
		Placeholder = (string) properties["placeholder"];
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es text's Font Family. It's have to be available in Unity.
	/// </summary>
	[BindableProperty]
	public Font FontFamily {
		get { return text.font; }
		set {
			placeholderText.font = text.font = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es text size.
	/// </summary>
	[BindableProperty]
	public int FontSize {
		get { return text.fontSize; }
		set {
			placeholderText.fontSize = text.fontSize = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es text decoration - underline, bold, italic.
	/// </summary>
	[BindableProperty]
	public FontStyle FontDecoration {
		get { return text.fontStyle; }
		set {
			text.fontStyle = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor Color {
		get { return new SerializableColor (text.color); }
		set {
			text.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es placeholder text color.
	/// </summary>
	[BindableProperty]
	public SerializableColor PlaceholderColor {
		get { return new SerializableColor (placeholderText.color); }
		set {
			placeholderText.color = value.GetColor ();
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Bindable Property. PasswordBox'es text alignment.
	/// </summary>
	[BindableProperty]
	public TextAnchor TextAlignment {
		get { return text.alignment; }
		set {
			text.alignment = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

	/// <summary>
	/// Read Only. Password value.
	/// </summary>
	public string Password {
		get { return inputField.text; }
	}

	/// <summary>
	/// Bindable Property. Placeholder Text.
	/// </summary>
	[BindableProperty]
	public string Placeholder {
		get { return placeholderText.text; }
		set {
			placeholderText.text = value;
			Window.BindingManager.CallBindingPropetyValueChange (this);
		}
	}

}