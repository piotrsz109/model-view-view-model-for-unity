﻿using System;

/// <summary>
/// Attribute, which marks classes, which are ViewModels.
/// </summary>
[AttributeUsage (AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
sealed class ViewModelAttribute : Attribute {

	/// <summary>
	/// Attribute, which marks classes, which are ViewModels.
	/// </summary>
	public ViewModelAttribute () { }

}