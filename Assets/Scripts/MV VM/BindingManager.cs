﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;

/// <summary>
/// MonoBehaviour script, which contains information about bindings and applies them.
/// </summary>
public class BindingManager : MonoBehaviour {

	/// <summary>
	/// Binding Flags to search for private non-static methods.
	/// </summary>
	const BindingFlags PrivateSearchFlags = BindingFlags.NonPublic | BindingFlags.Instance;

	/// <summary>
	/// Dictionary, which contains array of names bindable properties for every UIControl instantiated.
	/// </summary>
	protected Dictionary<UIControl, string[]> bindableProperties = new Dictionary<UIControl, string[]> ();
	/// <summary>
	/// Dictionary, which contains array of names bindable event for every UIControl instantiated.
	/// </summary>
	protected Dictionary<UIControl, string[]> bindableEvents = new Dictionary<UIControl, string[]> ();
	/// <summary>
	/// Dictionary, which contains name of property or handler, which is binded to bindable event or property.
	/// </summary>
	protected Dictionary<BindingData, string> allBindings = new Dictionary<BindingData, string> ();

	/// <summary>
	/// Array or reference to all view-models attached to this game object.
	/// </summary>
	[SerializeField] protected MonoBehaviour[] viewModels;

	/// <summary>
	/// Is called on Awake. Setups all bindings.
	/// </summary>
	private void Awake () {
		Setup ();
	}

	/// <summary>
	/// Method, which should be called, when bindable property, or binded property, are changing value.
	/// </summary>
	/// <param name="sender">Object, which is calling this method</param>
	/// <param name="isViewModel">Does caller has attribute of View Model?</param>
	/// <param name="bindName">Name of binded property.</param>
	public void CallBindingPropetyValueChange (object sender, bool isViewModel=false, [CallerMemberName] string bindName = "") {
		if(bindableProperties.Count > 0)
			SetProperty (sender, bindName, isViewModel);
	}

	/// <summary>
	/// Method, which setups, all bindings.
	/// </summary>
	public void Setup () {
		var controls = GetUIControlsWithoutWindow ();
		SetBindableProperties (controls);
		SetBindableEvents (controls);
		SetViewModels ();
		SetBindings ();
		AddEventsHandler ();
	}

	/// <summary>
	/// Sets for bindable property or Binded property, correct value.
	/// </summary>
	/// <param name="sender">Object, which sends the update of value.</param>
	/// <param name="bindName">Name of bindable property</param>
	/// <param name="isViewModel">Does sender is viewModel?</param>
	/// <exception cref="NotExistingBindablePropertyException">Throws when in BindingProperty Attribute you will pass the not existing Bindable Property Name.</exception>
	private void SetProperty (object sender, string bindName, bool isViewModel) {
		if (allBindings == null || allBindings.Count == 0)
			return;
		if (isViewModel && sender.GetType ().GetCustomAttribute (typeof (ViewModelAttribute)) != null) {
			var bindings = allBindings.Where (x => x.Value == bindName && !x.Key.IsEvent).ToArray ();
			foreach (var binding in bindings) {
				var bindValue = sender.GetType ().GetProperty (bindName).GetValue (sender);
				if (binding.Key == null)
					throw new NotExistingBindablePropertyException ($"Such bindable property as {binding.Key.BindableName} doesn't exist.");
				var bindableProperty = bindableProperties.First (x => x.Key.Name == binding.Key.ControlName);
				var key = bindableProperties.First (x => x.Key == bindableProperty.Key).Key;
				key.GetType ().GetProperty (binding.Key.BindableName).SetValue (key, bindValue);
			}
		} else if (!isViewModel && sender.GetType ().GetProperty (bindName) != null && sender is UIControl) {
			var binding = allBindings.FirstOrDefault (x => x.Key.BindableName == bindName && !x.Key.IsEvent);
			if (binding.Key == null)
				return;
			var bindValue = sender.GetType ().GetProperty (binding.Key.BindableName).GetValue (sender);
			var bindableProperty = bindableProperties.First (x => x.Key.Name == binding.Key.ControlName);
			var key = bindableProperties.First (x => x.Key == bindableProperty.Key).Key;
			var viewModel = viewModels.First (x => x.GetType ().GetProperty (binding.Value) != null);
			bindValue = bindableProperty.Key.GetType ().GetProperty (binding.Key.BindableName).GetValue (bindableProperty.Key);
			viewModel.GetType ().GetProperty (binding.Value).SetValue (viewModel, bindValue);
		} else if (isViewModel && sender.GetType ().GetCustomAttribute (typeof (ViewModelAttribute)) == null) {
			Debug.LogWarning ("You're not so clever as you think :p");
		}
	}

	/// <summary>
	/// To Bindable Events adds Binded handlers.
	/// </summary>
	/// <exception cref="NotExistingBindableEventException">Throws when in BindingEvent Attribute you will pass the not existing Bindable Event Name.</exception>
	/// <exception cref="ArgumentException">Throws when binded hander doesn't have right form.</exception>
	/// <exception cref="Exception">Throws when happens, something unexpected.</exception>
	private void AddEventsHandler () {
		var methodBindings = allBindings.Where (x => x.Key.IsEvent == true).ToArray ();
		foreach (var item in methodBindings) {
			var bindableEvent = bindableEvents.FirstOrDefault (x => x.Key.Name == item.Key.ControlName);
			if (bindableEvent.Key == null || !bindableEvent.Value.Contains (item.Key.BindableName))
				throw new NotExistingBindableEventException ($"Such bindable event as {item.Key.BindableName} doesn't exist.");
			var eventInfo = bindableEvent.Key.GetType ().GetEvent (item.Key.BindableName);
			var methodInfo = viewModels.First (x => x.GetType ().GetMethod (item.Value, PrivateSearchFlags) != null).GetType ().GetMethod (item.Value, PrivateSearchFlags);
			var viewModel = viewModels.First (x => x.GetType ().GetMethods (PrivateSearchFlags | BindingFlags.Public).Contains (methodInfo));
			try {
				eventInfo.RemoveEventHandler (bindableEvent.Key, Delegate.CreateDelegate (eventInfo.EventHandlerType, viewModel, methodInfo));
			} catch (Exception) { }
			try {
				eventInfo.AddEventHandler (bindableEvent.Key, Delegate.CreateDelegate (eventInfo.EventHandlerType, viewModel, methodInfo));
			} catch (ArgumentException) {
				throw new ArgumentException ("Return type, parameters of handler have to match to event handler type. Check also, does method is private.");
			} catch (Exception e) {
				throw new Exception ("Unexpected error! Who knows what happen?", e);
			}
		}
	}

	/// <summary>
	/// Sets value for bindableProperties. Gets all bindable properties from given controls.
	/// </summary>
	/// <param name="controls">List of UIControls</param>
	private void SetBindableProperties (List<UIControl> controls) {
		bindableProperties.Clear ();
		foreach (var item in controls) {
			var properties = GetPropertyInfo (item);
			bindableProperties.Add (item, GetPropertiesName (properties));
		}
	}

	/// <summary>
	/// Sets value for bindableEvents. Gets all bindable properties from given controls.
	/// </summary>
	/// <param name="controls">List of UIControls</param>
	private void SetBindableEvents (List<UIControl> controls) {
		bindableEvents.Clear ();
		foreach (var item in controls) {
			var properties = GetMethodInfo (item);
			bindableEvents.Add (item, GetEventsName (properties));
		}
	}

	/// <summary>
	/// Gets, all attached view models to game object, and sets the array.
	/// </summary>
	private void SetViewModels () {
		var temp = GetComponents<MonoBehaviour> ();
		viewModels = temp.Where (x => x.GetType ().CustomAttributes.Any (y => y.AttributeType == typeof (ViewModelAttribute))).ToArray ();
	}

	/// <summary>
	/// Gets binded properties and handlers from view-models
	/// </summary>
	private void SetBindings () {
		allBindings.Clear ();
		foreach (var viewModel in viewModels) {
			var viewModelsProperties = viewModel.GetType ().GetProperties ();
			var viewModelsMethods = viewModel.GetType ().GetMethods ().ToList();
			viewModelsMethods.AddRange (viewModel.GetType ().GetMethods (PrivateSearchFlags));
			AddBindedProperties (viewModelsProperties);
			AddBindedMethods (viewModelsMethods);
		}
	}

	/// <summary>
	/// Adds binded properties to dictionary.
	/// </summary>
	/// <param name="properties"></param>
	private void AddBindedProperties (ICollection<PropertyInfo> properties) {
		var bindedProperties = properties.Where (x => x.GetCustomAttributes().Count() >= 1).ToArray ();
		foreach (var property in bindedProperties) {
			var attributesData = (BindingPropertyAttribute[]) property.GetCustomAttributes (typeof (BindingPropertyAttribute));
			foreach (var attributeData in attributesData) {
				allBindings.Add (new BindingData (attributeData.ControlName, attributeData.BindablePropertyName, false), attributeData.PropertyName);
			}
		}
	}

	/// <summary>
	/// Adds binded handlers to dictionary.
	/// </summary>
	/// <param name="methods">Collection of MethodInfo, about handlers</param>
	private void AddBindedMethods (ICollection<MethodInfo> methods) {
		var bindedMethods = methods.Where (x => x.GetCustomAttribute(typeof(BindingEventHandlerAttribute)) != null).ToArray();
		foreach (var method in bindedMethods) {
			var attributeData = (BindingEventHandlerAttribute) method.GetCustomAttribute (typeof (BindingEventHandlerAttribute));
			allBindings.Add(new BindingData(attributeData.ControlName, attributeData.EventName), attributeData.MethodName);
		}
	}

	/// <summary>
	/// Returns all active UIControls from scene, except Window.
	/// </summary>
	/// <returns>List of UIControl from scene.</returns>
	private List<UIControl> GetUIControlsWithoutWindow () {
		return FindObjectsOfType<UIControl> ().Where (x => x.GetType () != typeof (Window)).ToList ();
	}

	/// <summary>
	/// Returns array about all bindable properties from give UIControl.
	/// </summary>
	/// <param name="control">UIControl</param>
	/// <returns>Array of PropertyInfo about bindable properties.</returns>
	private PropertyInfo[] GetPropertyInfo (UIControl control) {
		var properties = control.GetType ().GetProperties ();
		return properties.Where (x => x.CustomAttributes.Any (y => y.AttributeType == typeof (BindablePropertyAttribute))).ToArray();
	}

	/// <summary>
	/// Gets names, from give info.
	/// </summary>
	/// <param name="properties">PropertyInfo array, from which will be returned names.</param>
	/// <returns>String array, with properties' names.</returns>
	private string[] GetPropertiesName (PropertyInfo[] properties) {
		string[] output = new string[properties.Length];
		int i = 0;
		foreach (var item in properties) {
			output[i] = item.Name;
			i++;
		}
		return output;
	}

	/// <summary>
	/// Returns array about all bindable events from give UIControl.
	/// </summary>
	/// <param name="control">UIControl</param>
	/// <returns>Array of EventInfo about bindable events.</returns>
	private EventInfo[] GetMethodInfo (UIControl control) {
		var properties = control.GetType ().GetEvents ();
		return properties.Where (x => x.CustomAttributes.Any (y => y.AttributeType == typeof (BindableEventAttribute))).ToArray ();
	}

	/// <summary>
	/// Gets names, from give info.
	/// </summary>
	/// <param name="properties">EventInfo array, from which will be returned names.</param>
	/// <returns>String array, with events' names.</returns>
	private string[] GetEventsName (EventInfo[] methods) {
		string[] output = new string[methods.Length];
		int i = 0;
		foreach (var item in methods) {
			output[i] = item.Name;
			i++;
		}
		return output;
	}

}

/// <summary>
/// Class represents Bindable Property or Event with given UIControl name.
/// </summary>
[Serializable]
public class BindingData {

	/// <summary>
	/// Name of UIControl, which contains bindable property or event.
	/// </summary>
	public string ControlName { get; private set; }
	/// <summary>
	/// Name of Bindable property or event.
	/// </summary>
	public string BindableName { get; private set; }
	/// <summary>
	/// Does given Bindable is property or event.
	/// </summary>
	public bool IsEvent { get; private set; }

	/// <summary>
	/// Creates instance, which contains informations about UIControl, Bindable property or event, and does it is property or event.
	/// </summary>
	/// <param name="controlName">UIControl's name, which contains bindable.</param>
	/// <param name="bindableName">Name of bindable property or event.</param>
	/// <param name="isEvent">Does bindable is event or not.</param>
	public BindingData (string controlName, string bindableName, bool isEvent = true) {
		ControlName = controlName;
		BindableName = bindableName;
		IsEvent = isEvent;
	}

}
