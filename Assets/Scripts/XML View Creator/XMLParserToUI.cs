﻿using Newtonsoft.Json;
using System.Xml;
using System.Linq;
using UnityEngine;

/// <summary>
/// Parse XML to UI
/// </summary>
[RequireComponent(typeof(Window))]
[RequireComponent(typeof(BindingManager))]
public class XMLParserToUI : MonoBehaviour {

	/// <summary>
	/// String which contains name of json file with parring specific XML tag with Prefab
	/// </summary>
	[SerializeField] public const string ParringJsonFileName = "UI/Data/ParringData";
	/// <summary>
	/// XML File's Name which is gonna be used to generate UI.
	/// </summary>
	[SerializeField] protected string xmlFileName;
	/// <summary>
	/// Array of UIControlParing with all data read from file.
	/// </summary>
	[SerializeField] protected UIControlParing[] paringData;
	/// <summary>
	/// String with data read from XML file ready to process
	/// </summary>
	[SerializeField] protected string xmlCode;
	/// <summary>
	/// CSS parser for this XML Parser
	/// </summary>
	[SerializeField] protected CSSParserToUI cssParser;
	/// <summary>
	/// Use, when all of your's margins are %
	/// </summary>
	[SerializeField] public bool useScaledMarginsAndSize = false;

	/// <summary>
	/// Initialize the Parser
	/// </summary>
	public void Init () {
		InitParringData ();
		ReadXMLCode ();
	}

	/// <summary>
	/// Reads Parring Data from Json File and sets Parring Data Array
	/// </summary>
	/// <exception cref="System.ArgumentException">Throws, when you move parring data file or you delete him.</exception>
	protected void InitParringData () {
		TextAsset parringAsset = Resources.Load<TextAsset> (ParringJsonFileName);
		if (parringAsset != null) {
			string json = parringAsset.text;
			paringData = JsonConvert.DeserializeObject<UIControlParing[]> (json);
		} else throw new System.ArgumentException("There's no specified json file!");
	}

	/// <summary>
	/// Reads XML code from XML File
	/// </summary>
	/// <exception cref="System.ArgumentException">Throws when given file doesn't exists, or it's not xml file.</exception>
	protected void ReadXMLCode () {
		TextAsset xmlAsset = Resources.Load<TextAsset> (xmlFileName);
		if (xmlAsset != null) {
			xmlCode = xmlAsset.text;
		} else throw new System.ArgumentException ("There's no specified XML file!");
	}

	/// <summary>
	/// Removes old generated UI, and generates new based on read XML.
	/// </summary>
	public void UpdateView () {
		if (paringData == null) Init ();
		ReadXMLCode ();
		XmlDocument xmlDocument = new XmlDocument {
			InnerXml = xmlCode
		};
		var temp = GetComponent<RectTransform> ();
		cssParser = new CSSParserToUI (xmlDocument, ref temp, useScaledMarginsAndSize);
		RemoveUI ();
		GenerateUI ();
		GetComponent<BindingManager> ().Setup ();
	}

	/// <summary>
	/// Removes all children of Canvas.
	/// </summary>
	protected void RemoveUI () {
		for (int i = 0; i < transform.childCount; i++) {
			DestroyImmediate (transform.GetChild (i).gameObject);
		}
	}

	/// <summary>
	/// Method generate UI based on read XML.
	/// </summary>
	protected void GenerateUI () {
		XmlDocument xmlDocument = new XmlDocument {
			InnerXml = xmlCode
		};
		XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName ("UI");
		foreach (XmlNode node in xmlNodeList[0].ChildNodes) {
			GenerateUIOfChildren (node, transform);
		}
		GetComponent<Window> ().SetLayout ();
	}

	/// <summary>
	/// Method foreach through all Node children and call itself.
	/// </summary>
	/// <param name="xmlNode">XMLNode of children's parent</param>
	/// <param name="parent">Transform, to which is gonna be attached.</param>
	protected void GenerateUIOfChildren (XmlNode xmlNode, Transform parent) {
		if (xmlNode.Name.ToLower() == "style") return;
		Transform me = InstantiatePrefabFromXMLNode (xmlNode, parent);
		if (!xmlNode.HasChildNodes) return;
		foreach (XmlNode node in xmlNode.ChildNodes) {
			GenerateUIOfChildren (node, me);
		}
		me.GetComponent<UIControl> ().SetLayout ();
	}

	/// <summary>
	/// Sets Control GameObject's Name from Name attribute.
	/// </summary>
	/// <param name="control">GameObject of Control</param>
	/// <param name="xmlNode">XMLNode which represents Control</param>
	protected void SetNameForUIControl (GameObject control, XmlNode xmlNode) {
		XmlAttributeCollection attributeCollection = xmlNode.Attributes;
		if (attributeCollection["Name"] != null) {
			control.name = attributeCollection["Name"].Value;
		} else control.name = xmlNode.Name + $" {control.GetInstanceID()}";
	}

	/// <summary>
	/// Instantiate the right prefab.
	/// </summary>
	/// <param name="xmlNode">XMLNode is base to decide which prefab use.</param>
	/// <param name="parent">Transform, to which is gonna be attached.</param>
	/// <param name="uicontrol">Reference of UIControl script of parent, if the xmlNode is ScrollView. Else null</param>
	/// <exception cref="System.ArgumentException">Throws when in XML is used not allowed Node.</exception>
	protected Transform InstantiatePrefabFromXMLNode (XmlNode xmlNode, Transform parent) {
		if (paringData.Any (x => x.XMLTagName.ToLower() == xmlNode.Name.ToLower())) {
			GameObject prefab = Resources.Load<GameObject> (paringData.First (x => x.XMLTagName == xmlNode.Name.ToLower()).PrefabName);
			GameObject control = Instantiate (prefab, parent);
			if (parent.GetComponent<ScrollView> ())
				control.transform.SetParent (parent.GetChild (0).GetChild (0));
			if (cssParser != null && control.GetComponent<UIControl> () != null) {
				cssParser.SetupStyleToControl (xmlNode, control.GetComponent<UIControl> ());
			}
			SetNameForUIControl (control, xmlNode);
			if (parent.GetComponent<ScrollView> () == null)
				return control.transform;
			else return parent.GetChild (0).GetChild (0);
		} else throw new System.ArgumentException ("There's no available node such as: " + xmlNode.Name);
	}

}

/// <summary>
/// Represents Parring data about XML tag and prefab's name.
/// </summary>
[SerializeField]
public class UIControlParing {

	[SerializeField] private string xmlTagName;
	[SerializeField] private string prefabName;

	/// <summary>
	/// Constructor of UIControlParing's instance.
	/// </summary>
	/// <param name="xmlTagName">Name of XML tag.</param>
	/// <param name="prefabName">Name of Prefab.</param>
	public UIControlParing (string xmlTagName, string prefabName) {
		this.xmlTagName = xmlTagName;
		this.prefabName = prefabName;
	}

	/// <summary>
	/// String with XML Tag.
	/// </summary>
	public string XMLTagName {
		get { return xmlTagName; }
		set { xmlTagName = value; }
	}

	/// <summary>
	/// String with Prefab Name.
	/// </summary>
	public string PrefabName {
		get { return prefabName; }
		set { prefabName = value; }
	}

}
