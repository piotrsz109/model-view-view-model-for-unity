﻿using System;
using System.Runtime.CompilerServices;

/// <summary>
/// Marks the property, which has to bind to any Bindable Property.
/// </summary>
[AttributeUsage (AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
sealed class BindingPropertyAttribute : Attribute {

	/// <summary>
	/// Name of property to bind.
	/// </summary>
	public string PropertyName { get; private set; }
	/// <summary>
	/// Name of property, to which any property might be bind.
	/// </summary>
	public string BindablePropertyName { get; private set; }
	/// <summary>
	/// Name of UIControl, which contains Binding Property
	/// </summary>
	public string ControlName { get; private set; }

	/// <summary>
	/// Marks property to bind it's value with bindable property. 
	/// </summary>
	/// <remarks>You can only Bind method to events marked by BindableProperty attribute.</remarks>
	/// <param name="controlName">Name of UIControl, which contains Binding Property.</param>
	/// <param name="bindablePropertyName">Name of Bindable Property</param>
	/// <param name="name">Name of property.</param>
	public BindingPropertyAttribute (string controlName, string bindablePropertyName, [CallerMemberName] string name = "") {
		PropertyName = name;
		BindablePropertyName = bindablePropertyName;
		ControlName = controlName;
	}

}