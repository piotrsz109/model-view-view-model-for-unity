﻿using UnityEngine;
using System.IO;

[ViewModel]
public class SampleViewModel : MonoBehaviour {

	[BindingProperty("InfoLabel", "Text")]
	public string ContentString { get; set; }

	private void Start () {
		ContentString = File.ReadAllText (System.Environment.CurrentDirectory + "\\Hello.txt");
		Window.BindingManager.CallBindingPropetyValueChange (this, true, "ContentString");
	}

}