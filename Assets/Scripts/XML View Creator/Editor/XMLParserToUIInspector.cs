﻿using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Adds to Inspector Button: Update View and in Assets one button.
/// </summary>
[CustomEditor(typeof(XMLParserToUI))]
public class XMLParserToUIInspector : Editor {

	/// <summary>
	/// Called when InspectorGUI has to be showed.
	/// </summary>
	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();
		XMLParserToUI parser = (XMLParserToUI) target;
		EditorGUILayout.Space ();
		if (GUILayout.Button ("Update View")) {
			parser.UpdateView ();
		}
	}

	/// <summary>
	/// Adds to Assets/Create a new position with XML UI
	/// </summary>
	[MenuItem("Assets/Create/XML UI")]
	private static void AddNewUIFile () {
		var path = "";
		var obj = Selection.activeObject;
		if (obj == null) path = "Assets";
		else path = AssetDatabase.GetAssetPath (obj.GetInstanceID ());
		if (path.Length > 0) {
			if (!Directory.Exists (path)) {
				path = path.Substring (0, GetLastSlash (path)) + "/";
				Debug.Log (path);
			}
			try {
				if (path[path.Length - 1] != '/')
					path += '/';
				var stream = File.Create (path + "XMLView.xml");
				var data = Encoding.UTF8.GetBytes (
					$"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<UI xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
	 $"xsi:noNamespaceSchemaLocation=\"../UI/Data/UISchema.xsd\">");
				stream.Write (data, 0, data.Length);
				stream.Flush ();
				stream.Close ();
			} catch (System.Exception) {
				Debug.LogWarning ("Something went wrong. Check does in directory don't already exists file XMLView.xml");
			}
		}
	}

	/// <summary>
	/// Finds the last index of / in string
	/// </summary>
	/// <param name="s">String in which will be search.</param>
	/// <returns>Index</returns>
	private static int GetLastSlash (string s) {
		int output = -1;
		for (int i = 0; i < s.Length; i++) {
			if (s == "/") output = i;
		}
		return output;
	}

}
