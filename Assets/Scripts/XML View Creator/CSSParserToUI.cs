﻿using System.Collections.Generic;
using System.Xml;
using System.Linq;
using UnityEngine;

/// <summary>
/// Reads CSS and sends style properties to UIControl.
/// </summary>
public class CSSParserToUI {

	/// <summary>
	/// Describe path from Resources Folder to BaseValuesOfControl.txt file.
	/// </summary>
	public const string BaseValuesOfStyleFilePath = "UI/Data/BaseValuesOfControl";
	/// <summary>
	/// Contains all CSS Files attached to XML file.
	/// </summary>
	[SerializeField] protected List<string> cssData;
	/// <summary>
	/// Canvas's RectTransform reference.
	/// </summary>
	readonly RectTransform rectTransform;
	/// <summary>
	/// Does scaling UI is requested.
	/// </summary>
	readonly bool useScaling = false;
	/// <summary>
	/// Does css data is formated to be able read.
	/// </summary>
	bool isFormated = false;

	/// <summary>
	/// Creates instance of CSS Parser.
	/// </summary>
	/// <param name="xmlDocument">XML Document, from where will be read style nodes.</param>
	/// <param name="rectTransform">Canvas's RectTransform reference.</param>
	/// <param name="useScaling">Does scaling UI is requested.</param>
	public CSSParserToUI (XmlDocument xmlDocument, ref RectTransform rectTransform, bool useScaling) {
		this.rectTransform = rectTransform;
		this.useScaling = useScaling;
		cssData = new List<string> {
			LoadCssFileFromPath (BaseValuesOfStyleFilePath)
		};
		XmlNodeList nodeList = xmlDocument.GetElementsByTagName ("UI");
		if (nodeList.Count > 0) {
			foreach (XmlNode node in nodeList) {
				LookForStyleNodeInChildren (node);
			}
		}
		isFormated = false;
	}

	/// <summary>
	/// Recursive method, which looks from style node in children. 
	/// </summary>
	/// <param name="xmlNode">XML Node to be checked.</param>
	protected void LookForStyleNodeInChildren (XmlNode xmlNode) {
		if (xmlNode.Name.ToLower () == "style" && xmlNode.Attributes["Source"] != null) {
			cssData.Add (LoadCssFileFromPath (xmlNode.Attributes["Source"].Value));
			return;
		} else if (xmlNode.Name.ToLower () == "style" && xmlNode.Attributes["Source"] == null) {
			Debug.LogWarning ("There's style node which doesn't have the Source attribute set");
		}
		if (xmlNode.HasChildNodes) {
			foreach (XmlNode node in xmlNode.ChildNodes) {
				LookForStyleNodeInChildren (node);
			}
		}
	}

	/// <summary>
	/// Reads the text from given path.
	/// </summary>
	/// <param name="path">File path.</param>
	/// <returns>Text from file, or exception.</returns>
	/// <exception cref="System.ArgumentException">Throws when given in XML file style file doesn't exists.</exception>
	protected string LoadCssFileFromPath (string path) {
		if (Resources.Load<TextAsset> (path) != null) {
			return Resources.Load<TextAsset> (path).text;
		} else throw new System.ArgumentException ("There's no CSS file such as: " + path);
	}

	/// <summary>
	/// Setup for UIControl properties Dictionary.
	/// </summary>
	/// <param name="xmlNode">XML Node of UIControl.</param>
	/// <param name="control">UIControl where properties will be applied.</param>
	public void SetupStyleToControl (XmlNode xmlNode, UIControl control) {
		var output = GetCssProperties (xmlNode.Name.ToLower(), ObjectsInCSS.ControlCSS);
		if(xmlNode.Attributes["Class"] != null)
			AddOrReplaceInDictionary (ref output, GetCssProperties (xmlNode.Attributes["Class"].Value, ObjectsInCSS.ClassCSS));
		if (xmlNode.Attributes["Id"] != null)
			AddOrReplaceInDictionary (ref output, GetCssProperties (xmlNode.Attributes["Id"].Value, ObjectsInCSS.NameCSS));
		AddOrReplaceInDictionary (ref output, GetCssPropertiesFromAttributes(xmlNode));
		control.SetupProperties (ref output, useScaling);
	}

	/// <summary>
	/// Returns the properties read from all css files.
	/// </summary>
	/// <param name="search">Name of object in css.</param>
	/// <param name="attribute">What is that object.</param>
	/// <returns>Returns the Dictionary with all properties found for given arguments.</returns>
	protected Dictionary<string, object> GetCssProperties (string search, ObjectsInCSS attribute) {
		if (!isFormated) {
			FormatCssData ();
			isFormated = true;
		}
		Dictionary<string, object> output = new Dictionary<string, object> ();
		string realSearch = GetPrefix (attribute) + search;
		for (int i = 0; i < cssData.Count; i++) {
			int startIndex = cssData[i].IndexOf (realSearch);
			int endIndex = -1;
			if (startIndex >= 0)
				endIndex = cssData[i].IndexOf ("}", startIndex);
			if (endIndex != -1)
				AddOrReplaceInDictionary (ref output, GetProperties (cssData[i].Substring (startIndex, endIndex-startIndex)));
		}
		return output;
	}

	/// <summary>
	/// Formats all CSS Data.
	/// </summary>
	protected void FormatCssData () {
		for (int i = 0; i < cssData.Count; i++) {
			cssData[i] = cssData[i].Replace ("\n", "").Replace (" ", "").Replace ("\t", "");
			cssData[i] = cssData[i].Replace ("{", " {\n").Replace (";", "\n").Replace ("}", "}\n");
		}
	}

	/// <summary>
	/// Returns specific prefix for given names like: class -> .
	/// </summary>
	/// <param name="attribute">Attribute based on which data will be returned.</param>
	/// <returns>Returns string with prefix</returns>
	protected string GetPrefix (ObjectsInCSS attribute) {
		if (attribute == ObjectsInCSS.ControlCSS)
			return "";
		else if (attribute == ObjectsInCSS.ClassCSS)
			return ".";
		else
			return "#";
	}

	/// <summary>
	/// Merging modifications and origin dictionary.
	/// </summary>
	/// <param name="origin">In it will be saved modifications.</param>
	/// <param name="modifications">It will be saved in origin.</param>
	protected void AddOrReplaceInDictionary (ref Dictionary<string, object> origin, Dictionary<string, object> modifications) {
		foreach (string key in modifications.Keys) {
			if (origin.ContainsKey (key.ToLower()))
				origin[key.ToLower()] = modifications[key.ToLower()];
			else origin.Add (key.ToLower(), modifications[key]);
		}
	}

	/// <summary>
	/// Converts string of css object to properties and returns it with values in Dictionary.
	/// </summary>
	/// <param name="data">string of css object.</param>
	/// <returns>Dictionary of properties with values.</returns>
	protected Dictionary<string, object> GetProperties (string data) {
		Dictionary<string, object> output = new Dictionary<string, object> ();
		List<string> t = new List<string> (data.Split ('\n'));
		t.RemoveAt (0);
		for (int i = 0; i < t.Count; i++) {
			if (t[i][0] == '\r')
				t[i] = t[i].Remove (0, 1);
		}
		string[] props = t.ToArray();
		foreach (var item in props) {
			if (string.IsNullOrWhiteSpace (item)) continue;
			string[] temp = item.Split (':');
			output.Add (temp[0], GenerateValue (temp[1], temp[0]));
		}
		return output;
	}

	/// <summary>
	/// Generates values based on key and value.
	/// </summary>
	/// <param name="value">Value in string.</param>
	/// <param name="k">Key of value.</param>
	/// <returns>Value converted from string.</returns>
	protected object GenerateValue (string value, string k="") {
		string d = value.ToLower ();
		bool temp = false;
		k = k.ToLower ();
		if (d.Length >= 2 && d.Substring (d.Length - 2, 2) == "px") return float.Parse (d.Substring (0, d.Length - 2));
		else if (d.Length >= 1 && d[d.Length - 1] == '%') {
			int percent = int.Parse (d.Substring (0, d.Length - 1));
			if (k == "width" || k == "margin-left" || k == "margin-right") {
				return rectTransform.sizeDelta.x * percent / 100f;
			} else if (k == "height" || k == "margin-top" || k == "margin-bottom")
				return rectTransform.sizeDelta.y * percent / 100f;
			else return (float) percent;
		} else if (d[0] == '#') return new SerializableColor (d);
		else if (bool.TryParse (d, out temp)) return temp;
		else if (d.ToLower () == "null") return null;
		else if (k == "minimum" || k == "maximum") return float.Parse (d);
		else return value;
	}

	/// <summary>
	/// Gets attributes from XML Node and returns it in Dictionary.
	/// </summary>
	/// <param name="xmlNode">XML Node from which will be taken attributes</param>
	/// <returns>Dictionary with properties and values.</returns>
	protected Dictionary<string, object> GetCssPropertiesFromAttributes (XmlNode xmlNode) {
		Dictionary<string, object> fromAttributes = new Dictionary<string, object> ();
		foreach (XmlAttribute item in xmlNode.Attributes.ToList ().Where (x => x.Name != "Name" && x.Name != "Class")) {
			string temp = item.Name.ToLower ().Replace ("\n", "");
			fromAttributes.Add (temp, GenerateValue (item.Value, temp));
		}
		return fromAttributes;
	}

}

/// <summary>
/// Enum with possible objects in CSS.
/// </summary>
public enum ObjectsInCSS {
	ControlCSS,
	ClassCSS,
	NameCSS
}
