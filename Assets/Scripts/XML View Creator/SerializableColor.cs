﻿using System;
using UnityEngine;

/// <summary>
/// Serializable Color representation.
/// </summary>
[Serializable]
public class SerializableColor {

	/// <summary>
	/// Creates instance based on UnityEngine's Color.
	/// </summary>
	/// <param name="color">UnityEngine's Color.</param>
	public SerializableColor (Color color) {
		R = color.r;
		G = color.g;
		B = color.b;
		A = color.a;
	}

	/// <summary>
	/// Creates instance based on hexadecimal value.
	/// </summary>
	/// <param name="hex">String with hexadecimal value.</param>
	public SerializableColor (string hex) {
		if (hex.Length >= 3)
			R = GetDecNumber (hex.Substring (1, 2));
		if (hex.Length >= 5)
			G = GetDecNumber (hex.Substring (3, 2));
		if (hex.Length >= 7)
			B = GetDecNumber (hex.Substring (5, 2));
		if (hex.Length >= 9)
			A = GetDecNumber (hex.Substring (7, 2));
	}

	/// <summary>
	/// Creates instance based on UnityEngine's Color, but with NullExpresion.
	/// </summary>
	/// <param name="color">UnityEngine's Color.</param>
	public SerializableColor (Color? color) {
		if (color != null) {
			R = color.Value.r;
			G = color.Value.g;
			B = color.Value.b;
			A = color.Value.a;
		}
	}

	/// <summary>
	/// Returns int value of hexadecimal value.
	/// </summary>
	/// <param name="hex">Hexadecimal Value</param>
	/// <returns>Hexadecimal in int.</returns>
	private int GetDecNumber (string hex) {
		hex = hex.ToUpper ();
		int output = 0;
		for (int i = 0; i < hex.Length; i++) {
			if (hex[i] == 'A' || hex[i] == 'B' || hex[i] == 'C' || hex[i] == 'D' || hex[i] == 'E' || hex[i] == 'F') {
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'A') ? 10 : 0);
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'B') ? 11 : 0);
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'C') ? 12 : 0);
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'D') ? 13 : 0);
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'E') ? 14 : 0);
				output += (int) Mathf.Pow (16, hex.Length - 1 - i) * ((hex[i] == 'F') ? 15 : 0);
			} else output += (int) Mathf.Pow (16, hex.Length - 1 - i) * int.Parse (hex[i].ToString());
		}
		return output;
	}

	/// <summary>
	/// Red color amount.
	/// </summary>
	public float R { get; set; }

	/// <summary>
	/// Green color amount.
	/// </summary>
	public float G { get; set; }

	/// <summary>
	/// Blue color amount.
	/// </summary>
	public float B { get; set; }

	/// <summary>
	/// Alpha amount.
	/// </summary>
	public float A { get; set; } = 255;

	/// <summary>
	/// Get UnityEngine Color32.
	/// </summary>
	/// <returns>Color32 based on this color.</returns>
	public Color32 GetColor() {
		return new Color32 ((byte) R, (byte) G, (byte) B, (byte) A);
	}

	/// <summary>
	/// Overwritten ToString.
	/// </summary>
	/// <returns>Amount of Red, Green, Blue and Alpha.</returns>
	public override string ToString () {
		return string.Format ("R:{0} G:{1} B:{2} A:{3}", R, G, B, A);
	}

	/// <summary>
	/// Overwritten plus operator.
	/// </summary>
	/// <param name="left">First color to add.</param>
	/// <param name="right">Second color to add.</param>
	/// <returns>Sum of colors.</returns>
	public static SerializableColor operator + (SerializableColor left, SerializableColor right) {
		return new SerializableColor (new Color (left.R + right.R, left.G + right.G, left.B + right.B, left.A + right.A));
	}
	/// <summary>
	/// Overwritten minus operator.
	/// </summary>
	/// <param name="left">First color from where color will be removed..</param>
	/// <param name="right">Second color which is amount color to be removed..</param>
	/// <returns>Minus of colors.</returns>
	public static SerializableColor operator - (SerializableColor left, SerializableColor right) {
		return new SerializableColor (new Color (left.R - right.R, left.G - right.G, left.B - right.B, left.A - right.A));
	}

}